package model.logic;

import api.ITaxiTripsManager;
import model.vo.Taxi;
import model.vo.Service;
import model.data_structures.LinkedList;
import model.data_structures.SimpleLinkedList;

import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

public class TaxiTripsManager implements ITaxiTripsManager {

	// TODO
	// Definition of data model 

	SimpleLinkedList<Taxi> taxis = new SimpleLinkedList<Taxi>();

	SimpleLinkedList<Service> services = new SimpleLinkedList<>();

	public void loadServices (String serviceFile) {

		JsonParser parser = new JsonParser();

		try
		{
			String taxiTripsDatos = "./data/taxi-trips-wrvz-psew-subset-small.json";
			JsonArray array = (JsonArray) parser.parse(new FileReader(taxiTripsDatos));
			Taxi newTaxi = null;
			
			for(int i = 0; array != null && i < array.size(); i++)
			{
				JsonObject obj =(JsonObject) array.get(i);

				System.out.println("------------------------------------------------------------------------------------------------");
				System.out.println(obj);

				String company  = "NaN";
				if(obj.get("company") != null)
				{
					company = obj.get("company").getAsString();
				}

				String taxiId = "NaN";
				if(obj.get("taxi_id") != null)
				{
					taxiId = obj.get("taxi_id").getAsString();
				}

				String tripId = "NaN";
				if(obj.get("trip_id") != null)
				{
					tripId = obj.get("trip_id").getAsString();
				}

				int tripSeconds = 0;
				if(obj.get("trip_seconds") != null)
				{
					tripSeconds = obj.get("trip_seconds").getAsInt();
				}

				Double tripMiles = 0.0;
				if(obj.get("trip_miles") != null)
				{
					tripMiles = obj.get("trip_miles").getAsDouble();
				}

				Double tripTotal = 0.0;
				if(obj.get("trip_total") != null)
				{
					tripTotal = obj.get("trip_total").getAsDouble();
				}

				int dropOffCommunityArea = 0;
				if(obj.get("dropoff_community_area") != null)
				{
					dropOffCommunityArea = obj.get("dropoff_community_area").getAsInt();
				}
				
				newTaxi = new Taxi(taxiId, company);
				if(!taxis.contains(newTaxi))
				{
					taxis.add(newTaxi);
				}
				services.add(new Service(tripId, taxiId, tripSeconds, tripMiles, tripTotal, dropOffCommunityArea));
			}


		}
		catch (JsonIOException e1 ) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		catch (JsonSyntaxException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		catch (FileNotFoundException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}

		System.out.println("Inside loadServices with " + serviceFile);
	}

	@Override
	public LinkedList<Taxi> getTaxisOfCompany(String company) {
		// TODO Auto-generated method stub
			LinkedList<Taxi> taxisOfCompany = new SimpleLinkedList<Taxi>();
			
			for(int i = 0; i < taxis.size(); i++)
			{
				Taxi current = taxis.get(i);
				
				if(current.getCompany().compareTo(company) != 0)
				{
					taxisOfCompany.add(current);
				}
			}
		
		
		System.out.println("Inside getTaxisOfCompany with " + company);
		return taxisOfCompany;
	}

	@Override
	public LinkedList<Service> getTaxiServicesToCommunityArea(int communityArea) {
		// TODO Auto-generated method stub
		LinkedList<Service> servicesToCommunityArea = new SimpleLinkedList<>();
		
		for(int i = 0; i < services.size(); i++)
		{
			Service current = services.get(i);
			
			if(current.getDropOffCommunityArea() == communityArea)
			{
				servicesToCommunityArea.add(current);
			}
			
		
		}
		
		System.out.println("Inside getTaxiServicesToCommunityArea with " + communityArea);
		return servicesToCommunityArea;
	}


}
