package model.data_structures;

public class LinkedListNode<T>
{
	
	private T element;
	
	private LinkedListNode<T> next;

	
	public LinkedListNode(T element)
	{
		this.element = element;
	}
	
	public void setNext(LinkedListNode<T> next)
	{
		this.next = next;
	}
	
	public T removeElement()
	{
		T element = this.element;
		this.element = null;
		
		return element;
	}
	
	public T getElement()
	{
		return element;
	}
	
	public void changeElement(T element)
	{
		this.element = element;
	}
	
	/**
	 * Change the next Node of the actual Node.
	 * @param pNext New nextnode.
	 */
	public void changeNextNode(LinkedListNode<T> pNext)
	{
		next = pNext;
	}
	
	public LinkedListNode<T> getNext()
	{
		return next;
	}
	
	public boolean hasNext()
	{
		return this.next == null;
	}
}
