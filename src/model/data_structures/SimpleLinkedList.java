package model.data_structures;


import model.data_structures.LinkedList;

public class SimpleLinkedList<T extends Comparable<T>> implements LinkedList<T>
{
	private LinkedListNode<T> firstNode;

	private int size;

	private LinkedListNode<T> currentNode;

	public SimpleLinkedList() 
	{
		firstNode = null;
		currentNode = null;
		size = 0;
	}

	@Override
	public int size()
	{
		return size;
	}

	@Override
	public void listing() 
	{
		currentNode = firstNode;
	}

	@Override
	public LinkedListNode<T> getCurrent()
	{
		return currentNode;
	}

	@Override
	public void add(T element)
	{
		if( firstNode  == null)
		{
			firstNode = new LinkedListNode<T>(element);
			size++;
		}
		else
		{
			listing();
			while(next());

			currentNode.changeNextNode(new LinkedListNode<T>(element));
			size++;
		}

	}

	@Override
	public void delete(T pToDelete) throws Exception
	{
		if(firstNode!=null)
		{
			listing();
			if(currentNode.getElement().compareTo(pToDelete) == 0)
			{
				firstNode = firstNode.getNext();
			}
			else if(currentNode.getNext().getElement().compareTo(pToDelete) == 0)
			{
				currentNode.changeNextNode(currentNode.getNext().getNext());
			}
			else
			{
				boolean deleted = false;
				while(next() && !deleted)
				{
					if (currentNode.getNext()!= null? currentNode.getNext().getElement().compareTo(pToDelete) == 0:false)
					{
						currentNode.changeNextNode(currentNode.getNext().getNext());
						deleted = true;
					}
				}
				if(!deleted)
					throw new Exception ("El elemento a eliminar no existia");
			}
			size --;
		}
	}

	@Override
	public T get(T element)
	{
		listing();
		T found = null;
		
		while(next() && found == null)
		{
			if (currentNode.getElement().compareTo(element) == 0)
			{			
				found = currentNode.getElement();
			}
		}

		return found;
	}

	@Override
	public T get( int pPosition)
	{
		if(pPosition < 0 || pPosition > size)
		{
			throw new IndexOutOfBoundsException("Se est� pidiendo el indice: " + pPosition + " y el tama�o de la lista es de " + size);
		}

		listing();
		int posActual = 0;
		while(next() && posActual < pPosition)
		{
			posActual++;
		}

		return currentNode.getElement();
	}



	@Override
	public boolean next() 
	{
		boolean next = false;
		if (firstNode != null)
		{
			if (currentNode.getNext() != null)
			{
				next = true;
				currentNode = currentNode.getNext();	
			}
		}
		return next;		

	}

	public void clear()
	{
		firstNode = null;
		currentNode = null;
	}
	
	public boolean contains(T obj)
	{
		boolean contains = false;
		
		LinkedListNode<T> nodo = firstNode;
		
		while( nodo != null && contains)
		{
			if(nodo.getElement().equals(obj))
			{
				contains = true;
			}
			
			nodo = nodo.getNext();
		}

		return contains;
	}

}
