package test;

import static org.junit.Assert.*;
import model.data_structures.SimpleLinkedList;
import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;


public class SimpleLinkedListTest extends TestCase
{
	/**
	 * Lists for the three different configurations to test.
	 */
	private  static SimpleLinkedList<String> empty;   // empty list
	private static SimpleLinkedList<String> single;  // one-element list
	private static SimpleLinkedList<String> multi;   // multi-element list


	
	
	/**
	 *
	 */
	@Before
	public void setUp()
	{
		empty = new SimpleLinkedList<String>();         // []
		assertEquals(empty.size(), 0);

		single = new SimpleLinkedList<String>();        // [8]
		single.add("8");
		assertEquals( single.get(0), "8" );
		multi = new SimpleLinkedList<String>();         // [4 3 5 7 1 6]
		multi.add("6");
		multi.add("1");
		multi.add("7");
		multi.add("5");
		multi.add("3");
		multi.add("4");
		assertEquals( multi.get(0) + multi.get(1) + multi.get(2) + multi.get(3) + multi.get(4) + multi.get(5), "617534");
	}

	/**
	 * Test the add method
	 */
    @Test
    public void testAdd()
    {
            empty.add( "4" );
            assertEquals( empty.get(0), "4");

            single.add( "7" );
            assertEquals( single.get(0) + multi.get(1) , "87");

            multi.add( "9" );
            assertEquals( multi.get(0) + multi.get(1) + multi.get(2) + multi.get(3) + multi.get(4) + multi.get(5) + multi.get(6), "6175349");
    }
    
	/**
	 * Test the delete method
	 */
    @Test
    public void testDelete()
    {
    	try{
    	
    	single.delete("8");
    	assertEquals(single.size(), 0);
    	
    	multi.delete("7");
    	assertEquals(multi.get(0) + multi.get(1) + multi.get(2) + multi.get(3) + multi.get(4), "61534");
    	
    	}
    	catch(Exception e)
    	{
    		
    	}
    	
    }
    
	/**
	 * Test the get position method
	 */
    @Test
    public void testGetPosition()
    {
    	assertEquals(single.get(0), "8");
    	assertEquals(multi.get(3), "5");
    }
    
	/**
	 * Test the get element method
	 */
    @Test
    public void testGetElement()
    {
    	assertEquals(single.get("8"), "8");
    	assertEquals(multi.get("7"), "7");
    }
    

    
	
}